---
author: Alice Ricci
title: Illkirch-graffenstaden
project: paged.js sprint
book format: letter
book orientation: portrait
---



## Typefaces
### Happy Times at the IKOB New Game Plus Edition
Designed by Lucas Le Bihan
https://velvetyne.fr/fonts/happy-times/
SIL Open Font License, 1.1
